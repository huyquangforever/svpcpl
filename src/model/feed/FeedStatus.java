package model.feed;

public enum FeedStatus {
	Reviewing ("reviewing"), Approved ("approved"), Hidden ("hidden"), Deleted ("deleted");
	
	private String rawValue;
	
	FeedStatus(String rawValue) {
		this.rawValue = rawValue;
	}

	public String getRawValue() {
		return rawValue;
	}

	public void setRawValue(String rawValue) {
		this.rawValue = rawValue;
	}
}
