package model.comment;

import java.util.ArrayList;

import model.baseobject.BaseObject;

public class Comments extends BaseObject {
	private ArrayList<Comment> data;

	public Comments(ArrayList<Comment> data) {
		super();
		this.data = data;
	}

	public Comments() {
		super();
		this.data = new ArrayList<Comment>();
	}

	public ArrayList<Comment> getData() {
		return data;
	}

	public void setData(ArrayList<Comment> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Comments [data=" + data + "]";
	}
	
	
}
