package api;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import model.baseobject.*;
import model.code.*;
import model.comment.*;
import model.configs.*;
import model.feed.*;
import model.question.*;
import model.user.*;
import controller.*;

@Path("/v1")
public class API_V1 {

	/*
	 * Get configs API
	 */
	@GET
	@Path("/configs")
	@Produces(MediaType.APPLICATION_JSON)
	public Configs getConfigs(@Context HttpHeaders header)
	{
		Configs configs = new Configs();
		ConfigController controller = new ConfigController();
		configs = controller.getAllConfigs();
		return configs;
	}
	
	
}
