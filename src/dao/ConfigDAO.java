package dao;

import model.configs.Config;
import model.configs.Configs;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.client.MongoCollection;

import dbconnector.*;
public class ConfigDAO {
	private String collectionName = "Configs";
	public Configs getConfigs()
	{
		Configs configs = new Configs();
		MongoCollection<Document> configsCollection = DBConnector.sharedInstance().getDatabase().getCollection(collectionName);
		Block<Document> resultBlock = new Block<Document>() 
		{
			@Override
			public void apply(Document t) {
				Config config = new Config();
				String name = t.getString("name");
				String value = t.getString("value");
				config.setName(name);
				config.setValue(value);
				configs.getData().add(config);
			}
		};
		configsCollection.find().forEach(resultBlock);
		return configs;
	}
}
