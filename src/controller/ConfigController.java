package controller;

import dao.ConfigDAO;
import model.configs.Configs;

public class ConfigController {
	public Configs getAllConfigs()
	{
		ConfigDAO configDAO = new ConfigDAO();
		return configDAO.getConfigs();
	}
}
