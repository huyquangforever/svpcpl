package model.configs;

public class Config {
	private String name;
	private String value;
	public Config(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}
	public Config() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Config [name=" + name + ", value=" + value + "]";
	}
	
	
}
