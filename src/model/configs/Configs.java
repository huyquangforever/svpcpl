package model.configs;

import java.util.ArrayList;

import model.baseobject.BaseObject;


public class Configs extends BaseObject
{
	private ArrayList<Config> data;

	public Configs() 
	{
		super();
		data = new ArrayList<Config>();
	}

	public Configs( ArrayList<Config> data) 
	{
		super();
		this.data = data;
	}

	public ArrayList<Config> getData() {
		return data;
	}

	public void setData(ArrayList<Config> data) {
		this.data = data;
	}
	
}
