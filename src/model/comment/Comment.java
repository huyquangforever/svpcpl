package model.comment;

import org.bson.Document;

import model.user.UserCreated;

public class Comment 
{
	private String comment_id;
	private String content;
	private long date_created;
	private UserCreated user_created;
	private int like_count;
	
	public void parseDocument(Document document)
	{
		this.comment_id = document.getString("comment_id");
		this.content = document.getString("content");
		this.date_created = document.getLong("date_created");
		Document userCreated = document.get("user_created", Document.class);
		this.user_created.parseDocument(userCreated);
		this.like_count = document.getInteger("like_count", 0);
	}
	public Comment(String comment_id, String content, long date_created, UserCreated user_created, int like_count) {
		super();
		this.comment_id = comment_id;
		this.content = content;
		this.date_created = date_created;
		this.user_created = user_created;
		this.like_count = like_count;
	}
	public Comment() {
		super();
		this.user_created = new UserCreated();
	}
	public String getComment_id() {
		return comment_id;
	}
	public void setComment_id(String comment_id) {
		this.comment_id = comment_id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public long getDate_created() {
		return date_created;
	}
	public void setDate_created(long date_created) {
		this.date_created = date_created;
	}
	public UserCreated getUser_created() {
		return user_created;
	}
	public void setUser_created(UserCreated user_created) {
		this.user_created = user_created;
	}
	public int getLike_count() {
		return like_count;
	}
	public void setLike_count(int like_count) {
		this.like_count = like_count;
	}
	@Override
	public String toString() {
		return "Comment [comment_id=" + comment_id + ", content=" + content + ", date_created=" + date_created
				+ ", user_created=" + user_created + ", like_count=" + like_count + "]";
	}
	
	
}
