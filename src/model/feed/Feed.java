package model.feed;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import util.AppUtils;
import org.bson.Document;

import model.location.Location;
import model.user.UserCreated;

public class Feed 
{
	private String feed_id;
	private String content;
	private ArrayList<String> images;
	private String video_link;
	private String link;
	private UserCreated user_created;
	private long date_created;
	private long date_modified;
	private FeedStatus status;
	private int like_count;
	private int view_count;
	private int comment_count;
	private int share_count;
	private Location location;
	
	public void parseDocument(Document document)
	{
		this.feed_id = document.getString("feed_id");
		this.content = document.getString("content");
		this.video_link = document.getString("video_link");
		this.link = document.getString("link");
		Document userCreated = document.get("user_created", Document.class);
		this.user_created.parseDocument(userCreated);
		images = document.get("images", ArrayList.class);
		this.date_created = document.getLong("date_created");
		this.date_modified = document.getLong("date_modified");
		this.status.setRawValue(document.getString("status"));
		this.like_count = document.getInteger("like_count", 0);
		this.view_count = document.getInteger("view_count", 0);
		this.comment_count = document.getInteger("comment_count", 0);
		this.share_count = document.getInteger("share_count", 0);
		Document location = document.get("location", Document.class);
		this.location.parseDocument(location);
	}
	
	public Document toDocument()
	{
		Document userCreated = new Document();
		userCreated.append("user_id", this.user_created.getUser_id());
		userCreated.append("username", this.user_created.getUsername());
		userCreated.append("full_name", this.user_created.getFull_name());
		userCreated.append("avatar", this.user_created.getAvatar());
		Document location = new Document();
		location.append("lat", this.location.getLatitude());
		location.append("lon", this.location.getLongitude());
		
		String feed_id = "";
		feed_id += this.user_created.getUser_id();
		String date = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
		feed_id += date;
		feed_id = AppUtils.sha256(feed_id);
		
		Document feed = new Document();
		feed.append("feed_id", feed_id);
		feed.append("user_created", userCreated);
		feed.append("location", location);
		feed.append("content", this.content);
		feed.append("images", this.images);
		feed.append("video_link", this.video_link);
		feed.append("link", this.link);
		feed.append("date_created", this.date_created);
		feed.append("date_modified", this.date_modified);
		feed.append("status", this.status.getRawValue());
		feed.append("like_count", 0);
		feed.append("view_count", 0);
		feed.append("comment_count", 0);
		feed.append("share_count", 0);
		return feed;
	}
	
	public Feed() 
	{
		this.user_created = new UserCreated();
		
	}
	public Feed(String feed_id, String content, ArrayList<String> images, String video_link, String link,
			UserCreated user_created, long date_created, long date_modified, FeedStatus status, int like_count,
			int view_count, int comment_count, int share_count, Location location) {
		this.feed_id = feed_id;
		this.content = content;
		this.images = images;
		this.video_link = video_link;
		this.link = link;
		this.user_created = user_created;
		this.date_created = date_created;
		this.date_modified = date_modified;
		this.status = status;
		this.like_count = like_count;
		this.view_count = view_count;
		this.comment_count = comment_count;
		this.share_count = share_count;
		this.location = location;
	}
	public String getFeed_id() {
		return feed_id;
	}
	public void setFeed_id(String feed_id) {
		this.feed_id = feed_id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public ArrayList<String> getImages() {
		return images;
	}
	public void setImages(ArrayList<String> images) {
		this.images = images;
	}
	public String getVideo_link() {
		return video_link;
	}
	public void setVideo_link(String video_link) {
		this.video_link = video_link;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public UserCreated getUser_created() {
		return user_created;
	}
	public void setUser_created(UserCreated user_created) {
		this.user_created = user_created;
	}
	public long getDate_created() {
		return date_created;
	}
	public void setDate_created(long date_created) {
		this.date_created = date_created;
	}
	public long getDate_modified() {
		return date_modified;
	}
	public void setDate_modified(long date_modified) {
		this.date_modified = date_modified;
	}
	public FeedStatus getStatus() {
		return status;
	}
	public void setStatus(FeedStatus status) {
		this.status = status;
	}
	public int getLike_count() {
		return like_count;
	}
	public void setLike_count(int like_count) {
		this.like_count = like_count;
	}
	public int getView_count() {
		return view_count;
	}
	public void setView_count(int view_count) {
		this.view_count = view_count;
	}
	public int getComment_count() {
		return comment_count;
	}
	public void setComment_count(int comment_count) {
		this.comment_count = comment_count;
	}
	public int getShare_count() {
		return share_count;
	}
	public void setShare_count(int share_count) {
		this.share_count = share_count;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	@Override
	public String toString() {
		return "Feed [feed_id=" + feed_id + ", content=" + content + ", images=" + images + ", video_link=" + video_link
				+ ", link=" + link + ", user_created=" + user_created + ", date_created=" + date_created
				+ ", date_modified=" + date_modified + ", status=" + status + ", like_count=" + like_count
				+ ", view_count=" + view_count + ", comment_count=" + comment_count + ", share_count=" + share_count
				+ ", location=" + location + "]";
	}
	
	
}
