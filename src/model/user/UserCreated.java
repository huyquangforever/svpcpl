package model.user;

import org.bson.Document;

public class UserCreated 
{
	private String user_id;
	private String username;
	private String full_name;
	private String avatar;
	
	public void parseDocument(Document document)
	{
		this.user_id = document.getString("user_id");
		this.username = document.getString("username");
		this.full_name = document.getString("full_name");
		this.avatar = document.getString("avatar");
	}
	
	public UserCreated() {
		super();
	}
	public UserCreated(String user_id, String username, String full_name, String avatar) {
		super();
		this.user_id = user_id;
		this.username = username;
		this.full_name = full_name;
		this.avatar = avatar;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFull_name() {
		return full_name;
	}
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	@Override
	public String toString() {
		return "UserCreated [user_id=" + user_id + ", username=" + username + ", full_name=" + full_name + ", avatar="
				+ avatar + "]";
	}
	
	
}
