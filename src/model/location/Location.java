package model.location;

import org.bson.Document;

public class Location {
	private double latitude;
	private double longitude;
	
	public void parseDocument(Document document)
	{
		latitude = document.getDouble("lat");
		longitude = document.getDouble("lon");
	}
	
	public Location() {
		latitude = 0;
		longitude = 0;
	}
	public Location(float latitude, float longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	@Override
	public String toString() {
		return "Location [latitude=" + latitude + ", longitude=" + longitude + "]";
	}
	
	
}
