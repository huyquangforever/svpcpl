package dao;

import java.util.ArrayList;

import org.bson.BSON;
import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Sorts;

import dbconnector.DBConnector;
import model.feed.Feed;
import model.feed.FeedStatus;
import model.feed.Feeds;

public class FeedDAO 
{
	private String collectionName = "Feeds";
	
	/*
	 * get feeds with date, limit, page
	 * 
	 * date is timeinterval since 1970
	 * start page is 0
	 */
	public Feeds getFeeds(long date, int page, int limit)
	{
		Feeds feeds = new Feeds();
		MongoCollection<Document> collection = DBConnector.sharedInstance().getDatabase().getCollection(collectionName);
		Block<Document> resultsHandler = new Block<Document>() {

			@Override
			public void apply(Document t) {
				Feed feed = new Feed();
				feed.parseDocument(t);
				feeds.getData().add(feed);
			}
		};
		collection.find().sort(Sorts.descending("date_created")).limit(limit).skip(page*limit).forEach(resultsHandler);
		return feeds;
	}
	
	/*
	 * add a new feed
	 */
	public boolean addFeed(Feed feed)
	{
		feed.setStatus(FeedStatus.Reviewing);
		Document feedDocument = feed.toDocument();
		MongoCollection<Document> collection = DBConnector.sharedInstance().getDatabase().getCollection(collectionName);
		try
		{
			collection.insertOne(feedDocument);
		}catch (Exception ex)
		{
			return false;
		}
		return true;
	}
	
	/*
	 * delete a feed
	 * 
	 * using feed_id to delete feed
	 */
	public boolean deleteFeed(Feed feed)
	{
		Document feedDocument = new Document("feed_id", feed.getFeed_id());
		MongoCollection<Document> collection = DBConnector.sharedInstance().getDatabase().getCollection(collectionName);
		try
		{
			collection.deleteOne(feedDocument);
		}catch (Exception ex)
		{
			return false;
		}
		return true;
	}
	
	/*
	 * search feed
	 * 
	 * params: query
	 * results: Feeds 
	 */
	public Feeds findFeed(String query)
	{
		Feeds feeds = new Feeds();
		
		return feeds;
	}
}
