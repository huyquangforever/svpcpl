package dbconnector;
import java.util.ArrayList;

import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
public class DBConnector 
{
	private MongoClient client;
	private MongoDatabase database;
	
	private String user = "huyquang.ptit@gmail.com";
	private String databaseName = "db_pcpl";
	private String hostName = "localhost";
	private int port = 27017;
	private char[] password = "123456".toCharArray();
	
	private DBConnector() {
		try
		{
			MongoCredential credential = MongoCredential.createCredential(user, databaseName, password);
			ArrayList<MongoCredential> listCredential = new ArrayList<MongoCredential>();
			listCredential.add(credential);
			client = new MongoClient(new ServerAddress(hostName, port), listCredential);
			database = client.getDatabase(databaseName);
		}catch(Exception ex)
		{
			
		}
	}
	
	private static DBConnector instance = null;
	public static DBConnector sharedInstance()
	{
		if (instance == null)
		{
			instance = new DBConnector();
		}
		return instance;
	}
	
	public synchronized MongoDatabase getDatabase()
	{
		return database;
	}
}
