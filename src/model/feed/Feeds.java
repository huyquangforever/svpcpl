package model.feed;

import java.util.ArrayList;

import model.baseobject.BaseObject;

public class Feeds extends BaseObject 
{
	private ArrayList<Feed> data;
	
	public Feeds()
	{
		super();
		data = new ArrayList<Feed>();
	}

	public Feeds(ArrayList<Feed> data) 
	{
		super();
		this.data = data;
	}

	public ArrayList<Feed> getData() {
		return data;
	}

	public void setData(ArrayList<Feed> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Feeds [data=" + data + "]";
	}
	
}
