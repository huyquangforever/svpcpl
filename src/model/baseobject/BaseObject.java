package model.baseobject;

public class BaseObject {
	private MetaData meta_data;

	public BaseObject(MetaData meta_data) {
		this.meta_data = meta_data;
	}

	public BaseObject() 
	{
		this.meta_data = new MetaData();
	}

	public MetaData getMeta_data() {
		return meta_data;
	}

	public void setMeta_data(MetaData meta_data) {
		this.meta_data = meta_data;
	}

	@Override
	public String toString() {
		return "BaseObject [meta_data=" + meta_data + "]";
	}
	
	
}
class MetaData
{
	private String message;
	private int code;
	
	public MetaData(String message, int code) {
		this.message = message;
		this.code = code;
	}

	public MetaData() {
		this.message = "success";
		this.code = 200;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "MetaData [message=" + message + ", code=" + code + "]";
	}
	
	
}